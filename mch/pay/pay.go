package pay

import (
	"encoding/json"
	"log"
	"strconv"

	"gitee.com/teamlint/wechat/mp/core"
	"gitee.com/teamlint/wechat/mp/util"
	mchCore "gopkg.in/chanxuehong/wechat.v2/mch/core"
	"gopkg.in/chanxuehong/wechat.v2/mch/pay"
)

const (
	TradeTypeJsapi  string = "JSAPI"
	TradeTypeNative string = "NATIVE"
	TradeTypeMWeb   string = "MWEB"
)

// NativeUnifiedOrder 扫码支付统一下单
func NativeUnifiedOrder(body string, outTradeNo string, totalFree int64, spbillCreateIP string, notifyUrl string, productID string) (string, error) {
	client := mchCore.NewClient(core.Config.AppID, core.Config.MchID, core.Config.ApiKey, nil)
	req := &pay.UnifiedOrderRequest{Body: body, OutTradeNo: outTradeNo, TotalFee: totalFree, SpbillCreateIP: spbillCreateIP, NotifyURL: notifyUrl, TradeType: TradeTypeNative, ProductId: productID}
	resp, err := pay.UnifiedOrder2(client, req)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", err
	}
	return resp.CodeURL, nil
}

func JsapiSign(timeStamp, nonceStr, prepayId, signType string) string {
	return mchCore.JsapiSign(core.Config.AppID, timeStamp, nonceStr, prepayId, signType, core.Config.ApiKey)
}

// UnifiedOrder 统一下单
func UnifiedOrder(body string, outTradeNo string, totalFree int64, spbillCreateIP string, notifyUrl string, tradeType string, oepnID string) (string, string, error) {
	client := mchCore.NewClient(core.Config.AppID, core.Config.MchID, core.Config.ApiKey, nil)
	req := &pay.UnifiedOrderRequest{Body: body, OutTradeNo: outTradeNo, TotalFee: totalFree, SpbillCreateIP: spbillCreateIP, NotifyURL: notifyUrl, TradeType: tradeType, OpenId: oepnID}
	resp, err := pay.UnifiedOrder2(client, req)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", "", err
	}
	return resp.PrepayId, resp.TradeType, nil
}

// MWebUnifiedOrder H5支付统一下单
func MWebUnifiedOrder(body string, outTradeNo string, totalFree int64, spbillCreateIP string, notifyUrl string, wapUrl string, wapName string) (string, error) {
	client := mchCore.NewClient(core.Config.AppID, core.Config.MchID, core.Config.ApiKey, nil)
	h5Info := struct {
		Type    string `json:"type"`
		WapUrl  string `json:"wap_url"`
		WapName string `json:"wap_name"`
	}{
		Type:    "Wap",
		WapUrl:  wapUrl,
		WapName: wapName,
	}
	sceneInfo, _ := json.Marshal(map[string]interface{}{"h5_info": h5Info})
	req := &pay.UnifiedOrderRequest{Body: body, OutTradeNo: outTradeNo, TotalFee: totalFree, SpbillCreateIP: spbillCreateIP, NotifyURL: notifyUrl, TradeType: TradeTypeMWeb, SceneInfo: string(sceneInfo)}
	resp, err := pay.UnifiedOrder2(client, req)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", err
	}
	return resp.MWebURL, nil
}

// Refund      退款
//  tradeNo:    商户订单号，需保持唯一性 (只能是字母或者数字，不能包含有符号)
//  refundNo:   退款单号
//  totalFee:   订单金额
//  refundFee:  退款金额
// return
//  bool        是否成功
//  string      微信退款单号，如果失败则为错误消息
func Refund(tradeNo string, refundNo string, totalFee int64, refundFee int64) (bool, string) {
	httpClient, err := mchCore.NewTLSHttpClient("/root/cert/ac_cert.pem", "/root/cert/ac_key.pem")
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return false, err.Error()
	}
	client := mchCore.NewClient(core.Config.AppID, core.Config.MchID, core.Config.ApiKey, httpClient)
	req := map[string]string{
		"appid":         core.Config.AppID,
		"mch_id":        core.Config.MchID,
		"nonce_str":     util.NonceStr(),
		"sign":          "",
		"out_trade_no":  tradeNo,
		"out_refund_no": refundNo,
		"total_fee":     strconv.FormatInt(totalFee, 10),
		"refund_fee":    strconv.FormatInt(refundFee, 10),
	}
	req["sign"] = mchCore.Sign(req, core.Config.ApiKey, nil)
	resp, err := pay.Refund(client, req)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		switch v := err.(type) {
		case *mchCore.Error:
			return false, v.ReturnMsg
		case *mchCore.BizError:
			return false, v.ErrCode + ": " + v.ErrCodeDesc
		default:
			return false, v.Error()
		}
	}
	return true, resp["refund_id"]
}
