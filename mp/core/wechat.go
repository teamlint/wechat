package core

import (
	"gopkg.in/chanxuehong/wechat.v2/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/jssdk"
)

type WeChat struct {
	TokenServer  *core.DefaultAccessTokenServer
	TicketServer *jssdk.DefaultTicketServer
	AppID        string
	AppSecret    string
	MchID        string
	ApiKey       string
}

var Config *WeChat

func Setup(appID, appSecret, mchID, apiKey string) {
	accessTokenServer := core.NewDefaultAccessTokenServer(appID, appSecret, nil)
	client := core.NewClient(accessTokenServer, nil)
	Config = &WeChat{accessTokenServer, jssdk.NewDefaultTicketServer(client), appID, appSecret, mchID, apiKey}
}
