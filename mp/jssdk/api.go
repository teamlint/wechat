package jssdk

import (
	"encoding/json"
	"gitee.com/teamlint/wechat/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/jssdk"
	"gopkg.in/chanxuehong/wechat.v2/util"
	"log"
	"strconv"
	"time"
)

func JSSDKConfig(url string) (string, error) {
	nonceStr := util.NonceStr()
	timestamp := time.Now().Unix()
	timestampStr := strconv.FormatInt(timestamp, 10)
	ticket, err := core.Config.TicketServer.Ticket()
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", err
	}
	json, err := json.Marshal(map[string]interface{}{
		"appId":     core.Config.AppID,
		"timestamp": timestamp,
		"nonceStr":  nonceStr,
		"signature": jssdk.WXConfigSign(ticket, nonceStr, timestampStr, url),
	})
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", err
	}
	return string(json), nil
}