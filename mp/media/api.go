package media

import (
	"log"
	"os"
	"path"

	tlCore "gitee.com/teamlint/wechat/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/media"
)

func Download(mediaId, filepath string) (int64, error) {
	dir := path.Dir(filepath)
	err := os.MkdirAll(dir, os.ModePerm) //生成多级目录
	if err != nil {
		return 0, err
	}
	client := core.NewClient(tlCore.Config.TokenServer, nil)
	written, err := media.Download(client, mediaId, filepath)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return 0, err
	}
	return written, nil
}
