package menu

import (
	tlCore "gitee.com/teamlint/wechat/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/menu"
)

type (
	Menu   = menu.Menu
	Button = menu.Button
)

func CreateMenu(wxMenu *Menu) error {
	client := core.NewClient(tlCore.Config.TokenServer, nil)
	return menu.Create(client, wxMenu)
}
