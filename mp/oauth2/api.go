package oauth2

import (
	"log"

	"gitee.com/teamlint/wechat/mp/core"

	mpoauth2 "gopkg.in/chanxuehong/wechat.v2/mp/oauth2"
	"gopkg.in/chanxuehong/wechat.v2/oauth2"
)

type Scope string

const (
	ScopeBase     Scope = "snsapi_base"
	ScopeUserInfo Scope = "snsapi_userinfo"
)

func AuthCodeUrl(redirectUri string, scope Scope) string {
	return mpoauth2.AuthCodeURL(core.Config.AppID, redirectUri, string(scope), "a")
}

func ExchangeToken(code string) (string, string, error) {
	oauth2Endpoint := mpoauth2.NewEndpoint(core.Config.AppID, core.Config.AppSecret)
	oauth2Client := oauth2.Client{Endpoint: oauth2Endpoint}
	token, err := oauth2Client.ExchangeToken(code)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", "", err
	}
	return token.OpenId, token.AccessToken, nil
}

func GetUserInfo(accessToken, openId string) (string, string, error) {
	const lang = "zh_CN"
	user, err := mpoauth2.GetUserInfo(accessToken, openId, lang, nil)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", "", err
	}
	return user.Nickname, user.HeadImageURL, nil
}
