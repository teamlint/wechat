package qrcode

import (
	"log"

	tlCore "gitee.com/teamlint/wechat/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/qrcode"
)

// CreateStrSceneTempQrcode 生成临时二维码
func CreateStrSceneTempQrcode(sceneStr string, expireSeconds int) (string, string, int, error) {
	client := core.NewClient(tlCore.Config.TokenServer, nil)
	code, err := qrcode.CreateStrSceneTempQrcode(client, sceneStr, expireSeconds)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", "", 0, err
	}
	return code.URL, code.Ticket, code.ExpireSeconds, nil
}
