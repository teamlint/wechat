package util

import (
	"log"
	"strings"

	"github.com/chanxuehong/rand"

	tlCore "gitee.com/teamlint/wechat/mp/core"
	"gopkg.in/chanxuehong/wechat.v2/mp/base"
	"gopkg.in/chanxuehong/wechat.v2/mp/core"

	"gopkg.in/chanxuehong/wechat.v2/util"
)

func WXBrowser(userAgent string) bool {
	_, _, _, _, err := util.WXVersion(userAgent)
	if err != nil {
		i := strings.LastIndex(userAgent, "MicroMessenger/")
		if i == -1 {
			log.Printf("WXError:\n不是有效的微信浏览器 User-Agent: %s\n", userAgent)
			return false
		}
	}
	return true
}

func ShortUrl(longUrl string) (string, error) {
	client := core.NewClient(tlCore.Config.TokenServer, nil)
	url, err := base.ShortURL(client, longUrl)
	if err != nil {
		log.Printf("WXError:\n%v\n", err)
		return "", err
	}
	return url, nil
}

func NonceStr() string {
	return string(rand.NewHex())
}
